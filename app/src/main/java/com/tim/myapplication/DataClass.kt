package com.tim.myapplication

data class DataClass(val systolic:Int, val diastolic:Int, val pulse:Int)