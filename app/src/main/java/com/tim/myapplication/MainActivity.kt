package com.tim.myapplication

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val data = LineData(generateLineData())

        val xAxis: XAxis = chart1.xAxis
        xAxis.textSize = 11f
        xAxis.setDrawGridLines(true)
        xAxis.setDrawAxisLine(true)
        xAxis.position = XAxis.XAxisPosition.BOTTOM

        val leftAxis: YAxis = chart1.axisLeft
        leftAxis.textColor = ColorTemplate.getHoloBlue()
        leftAxis.axisMaximum = 240f
        leftAxis.axisMinimum = 0f
        leftAxis.setDrawGridLines(true)
        leftAxis.isGranularityEnabled = true

//        val rightAxis: YAxis = chart1.axisRight
//        rightAxis.textColor = Color.RED
//        rightAxis.axisMaximum = 120f
//        rightAxis.axisMinimum = 0f
//        rightAxis.setDrawGridLines(true)
//        rightAxis.setDrawZeroLine(true)
//        rightAxis.isGranularityEnabled = false

        chart1.data = data
        chart1.legend.isEnabled = false
        chart1.xAxis.setAvoidFirstLastClipping(true)
        chart1.xAxis.axisMaximum = 80.0f + 0.5f
        chart1.axisRight.isEnabled = false
        chart1.description.isEnabled = false;
        chart1.invalidate()

    }

    private fun generateLineData(): ArrayList<ILineDataSet> {

        val dataSets = ArrayList<ILineDataSet>()

        val data = Utils.getData()

        val values = ArrayList<Entry>()
        var x = 0f
        for (item in data.entries) {
            for ((index, value) in item.value.withIndex()) {
                x += 10.0f
                values.add(Entry(x, value.pulse.toFloat()))
            }
        }

        val d = LineDataSet(values, "DataSet $x")
        d.lineWidth = 3.0f
//        d.axisDependency = YAxis.AxisDependency.RIGHT
        d.circleRadius = 4f
        d.setDrawValues(false)
        dataSets.add(d)

        var x1 = 0f

        for (item in data.entries) {
            for (value in item.value) {
                x1 += 10.0f
                val verticalValues = ArrayList<Entry>()
                verticalValues.add(Entry(x1, value.systolic.toFloat()))
                verticalValues.add(Entry(x1, value.diastolic.toFloat()))

                val d1 = LineDataSet(verticalValues, "")
                d1.lineWidth = 5f
                d1.circleRadius = 5f
                d1.color = ContextCompat.getColor(this, R.color.light_gray)
                d1.setCircleColors(ContextCompat.getColor(this, R.color.graph_orange),
                    ContextCompat.getColor(this, R.color.colorAccent))
                d1.fillColor = ContextCompat.getColor(this, R.color.graph_orange)
                d1.axisDependency = YAxis.AxisDependency.LEFT
                d1.mode = LineDataSet.Mode.LINEAR
                d1.setDrawValues(false)
                d1.setDrawCircleHole(false)
                dataSets.add(d1)
                Log.e("TAG", "-----" + dataSets.size)
            }
        }
        return dataSets
    }

}
