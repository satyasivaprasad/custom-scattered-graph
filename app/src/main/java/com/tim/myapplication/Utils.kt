package com.tim.myapplication

object Utils {

    fun getData(): HashMap<String, ArrayList<DataClass>> {

        val map = HashMap<String, ArrayList<DataClass>>()

        val list  = arrayListOf<DataClass>()

        list.add(DataClass(120, 80, 95))
        list.add(DataClass(140, 90, 110))
        list.add(DataClass(100, 80, 89))

        map["Day 1"] = list

        val list1  = arrayListOf<DataClass>()
        list1.add(DataClass(160, 100, 120))
        list1.add(DataClass(130, 100, 110))

        map["Day 2"] = list1

        val list2  = arrayListOf<DataClass>()
        list2.add(DataClass(120, 80, 90))
        list2.add(DataClass(140, 90, 100))
        list2.add(DataClass(105, 80, 86))
        map["Day 3"] = list2


        return map
    }
}